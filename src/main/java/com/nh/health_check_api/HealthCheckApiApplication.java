package com.nh.health_check_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HealthCheckApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(HealthCheckApiApplication.class, args);
    }

}
