package com.nh.health_check_api.service;

import com.nh.health_check_api.entity.Health;
import com.nh.health_check_api.model.HealthItem;
import com.nh.health_check_api.model.HealthRequest;
import com.nh.health_check_api.model.HealthResponse;
import com.nh.health_check_api.model.HealthStatusChangeRequest;
import com.nh.health_check_api.repository.HealthRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HealthService {
    private final HealthRepository healthRepository;

    public void setHealth(HealthRequest request) {
        Health addData = new Health();
        addData.setDateCreate(LocalDate.now());
        addData.setName(request.getName());
        addData.setHealthStatus(request.getHealthStatus());
        addData.setIsChronicDisease(request.getIsChronicDisease());
        addData.setEtcMemo(request.getEtcMemo());

        healthRepository.save(addData);
    }
    public List<HealthItem> getHealth(){
        List<Health> originList = healthRepository.findAll();

        List<HealthItem> result = new LinkedList<>();

        for (Health health : originList){
            HealthItem addItem = new HealthItem();
            addItem.setId(health.getId());
            addItem.setDateCreate(health.getDateCreate());
            addItem.setName(health.getName());
            addItem.setIsGoHome(health.getHealthStatus().getIsGoHome());

            result.add(addItem);
        }

        return result;
    }

    public HealthResponse getHealth(long id){
        Health originData = healthRepository.findById(id).orElseThrow();

        HealthResponse  response = new HealthResponse();
        response.setId(originData.getId());
        response.setHealthStatusName(originData.getHealthStatus().getName());

        response.setIsGoHomeName(originData.getHealthStatus().getIsGoHome() ? "예":"아니요");

        response.setDateCreate(originData.getDateCreate());
        response.setName(originData.getName());

        response.setIsChronicDisease(originData.getIsChronicDisease() ? "예":"아니요");

        response.setHealthStatusName(originData. getHealthStatus().getName());

        return response;
    }
    public void putHealthStatus(long id, HealthStatusChangeRequest request) {
        Health originData = healthRepository.findById(id).orElseThrow();
        originData.setHealthStatus(request.getHealthStatus());

        healthRepository.save(originData);
    }
}
