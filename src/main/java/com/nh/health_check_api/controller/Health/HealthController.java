package com.nh.health_check_api.controller.Health;

import com.nh.health_check_api.model.HealthItem;
import com.nh.health_check_api.model.HealthRequest;
import com.nh.health_check_api.model.HealthResponse;
import com.nh.health_check_api.model.HealthStatusChangeRequest;
import com.nh.health_check_api.service.HealthService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/health")
public class HealthController {
    private final HealthService healthService;

    @PostMapping("/people")
    public String setHealth(@RequestBody HealthRequest request) {
        healthService.setHealth(request);

        return "OK";
    }
    @GetMapping("/all")
    public List<HealthItem> getHealths() {return healthService.getHealth();}

    @GetMapping("/detail/{id}")
    public HealthResponse getHealth(@PathVariable long id){
        return healthService.getHealth(id);
    }
    @PutMapping("/status/{id}")
    public String putHealthStatus(@PathVariable long id, @RequestBody HealthStatusChangeRequest request){
        healthService.putHealthStatus(id, request);

        return "OK";
    }

}
