package com.nh.health_check_api.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class HealthResponse {
    private Long id;
    private String healthStatusName;
    private String isGoHomeName;
    private LocalDate dateCreate;
    private String name;
    private String isChronicDisease;
    private String etcMemo;
}
