package com.nh.health_check_api.model;

import enums.HealthStatus;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class HealthItem {
    private Long id;
    private LocalDate dateCreate;
    private String name;
    private String healthStatus;
    private Boolean isChronicDisease;
    private Boolean isGoHome;
}
