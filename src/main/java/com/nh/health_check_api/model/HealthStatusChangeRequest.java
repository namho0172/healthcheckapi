package com.nh.health_check_api.model;

import enums.HealthStatus;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HealthStatusChangeRequest {
    @Enumerated(value = EnumType.STRING)
    private HealthStatus healthStatus;
}
