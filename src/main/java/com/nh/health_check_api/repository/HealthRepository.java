package com.nh.health_check_api.repository;

import com.nh.health_check_api.entity.Health;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HealthRepository extends JpaRepository<Health, Long> {

}
